# Binance Tools

UI for [beptools.org](https://beptools.org)

## Setup

```bash
yarn install
```

## Start

```bash
yarn start
```

## Deploy

```bash
yarn deploy
```

To deploy production...

```bash
ENVIRONMENT=prod yarn deploy
```

## Development

- [react-context-devtool](https://github.com/deeppatel234/react-context-devtool)
  - React Context Devtool
- [React Context DevTool](https://chrome.google.com/webstore/detail/react-context-devtool/oddhnidmicpefilikhgeagedibnefkcf)
  - Chrome extension for React Context Devtool
